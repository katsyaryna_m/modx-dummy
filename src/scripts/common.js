'use strict';
/* безопасный target="_blank" (см. https://habr.com/ru/post/282880/) */
$(document).on('click', '[target="_blank"]', function(e) {e.preventDefault();var otherWindow = window.open();otherWindow.opener = null;otherWindow.location = $(this).attr('href');});
/* \\ безопасный target="_blank" (см. https://habr.com/ru/post/282880/) */

$(document).ready(function() {
  //добавление класса для стилизации шапки при прокрутке
  $(window).on('scroll load touchmove', function() {
    initStickyHeader( $(this) );
  });

  function initStickyHeader(el) {
    if( el.scrollTop() > 20 ) {
      $('.header').addClass('sticking');
    } else{
      $('.header').removeClass('sticking');
    };
  };

  //инициализация маски для телефона
  $('input[name="phone"]').mask('+7 (999) 999-99-99');

  //инициализация popup
  $('[data-popup]').magnificPopup();

  //инициализация popup gallery
  (function initPopupGallery() {
    var gallery = document.querySelector('[data-popup-gallery]');
    if(!gallery) return;

    $('[data-popup-gallery]').each(function() {
      $(this).magnificPopup({
          delegate: 'a',
          type: 'image',
          tLoading: 'Загрузка изображения #%curr%...',
          closeOnContentClick: false,
          closeBtnInside: false,
          mainClass: 'mfp-img-mobile',
          gallery: {
              enabled: true,
              navigateByImgClick: true,
              preload: [0,1], // Will preload 0 - before current, and 1 after the current image
              tCounter: '' // markup of counter
          },
          image: {
              tError: '<a href="%url%">Изображение #%curr%</a> не может быть загружено.',
          }
      });
    });
  })();

  //сворачивание/разворачивание блоков
  (function toogleBlock() {
    var toggleEl = document.querySelector('[data-toggle]');
    if(!toggleEl) return;

    $(document).on('click', '[data-toggle]', function(e) {
        e.preventDefault();

        $( $(this).data('toggle') ).toggleClass('d-none');
        $(this).toggleClass('active');
    });
  })();

  //табы
  (function showTabs(){
    var parent = $('[data-floor-scheme]');
    //назначаем первом кнопке класс active по умолчанию
    $('[data-floor-btn]:first-child').addClass('active');
    //показываем блок в соответствии с активной кнопкой
    $('[data-floor-btn]').each(function() {
      if( $(this).hasClass('active')) {
        var i = $(this).data('floor-btn');
        parent.find('[data-floor-img="' + i + '"]').fadeIn();
      }
    });
    //переключение изображений
    $(document).on('click', '[data-floor-btn]', function(e) {
      e.preventDefault();
      var floorNumber = $(this).data('floor-btn');
      var floorImg = $('[data-floor-img="' + floorNumber + '"]');

      $(this).addClass('active').siblings().removeClass('active');
      floorImg.fadeIn(700).siblings().fadeOut(300);
    });
  })();

  //инициализация слайдерa
  (function initMainpageSliders() {
    var mainpageSlider = new Swiper('#mainpage_slider', {
      loop: true,
      autoplay: {
        delay: 3500,
        disableOnInteraction: false,
      },
    });
  })();

  //анимация счетчика на главной странице
  (function animateNumber() {
      if( $(window).width() < 767 ) return;

       var show = true;
       var countbox = '[data-statistics]';

       $(window).on('scroll load touchmove', function() {
         if( $(countbox).lenght == 0 || !show ) return false; // отменяем показ анимации, если она уже была выполнена

         $(countbox).each(function() {

          var w_top = $(window).scrollTop(); // количество пикселей, на которое была прокручена страница
          var e_top = $(this).offset().top;     // расстояние от блока со счетчиками до верха всего документа
          var w_height = $(window).height();    // высота окна браузера

          if( w_top <= e_top && w_top + w_height >= e_top ) {
            var time = 4;
            var i = 0;
            var number = $(this).data('to');
            var that = $(this);
            var int = setInterval(function(){
                  if (i <= number) {
                    that.html(i);
                  }
                  else {
                    clearInterval(int);
                  }

                  if(number < 50) {
                    i++;
                  } else if(number >= 50 && number < 500) {
                    i += 5;
                  } else {
                    i += 50;
                  }

                },30);

            show = false;
          }
         });
       });
  })();

  //анимация input label в формах
  (function animateFormPlaceholders() {
    $('[data-form-input]').focusin(function(){
      $(this).siblings( $('.form__input-label') ).addClass('small');
    });

    $('[data-form-input]').focusout(function(){
      var val = $(this).val();

      if( val == '' || (val.replace( /[^\d+?()]/g, '')) == '+7()') {
        $(this).siblings( $('.form__input-label') ).removeClass('small');
      }
    });
  })();

  //скролл от футера к шапке сайта
  (function scrollToHeader() {
    $(document).on('click', '#btn-up', function(e) {
      e.preventDefault();
      $('html,body').animate({scrollTop:0 + "px"},{duration: 1E3});
    });
  })();

  //обертка для таблиц
  $('table').wrap("<div class='table-wrap'></div>");

  //функционал для мобилки
  if( $(window).width() < 767 ) {
    // мобильное меню
    (function showMobileMenu() {
      $(document).on('click', '#btn_menu_mobile', function(e) {
        e.preventDefault;

        $(this).toggleClass('close');
        $('.header__nav-wrap').fadeToggle();

        if( $('.header__btn-menu').hasClass('close')) {
           $('html,body').css('overflow', 'hidden');
           $('html,body').css('height', '110vh');
       } else{
           $('html,body').css('overflow', 'auto');
           $('html,body').css('height', 'auto');
       }
     });
    })();
  }

  // ajax запросы при нажатии на кнопки с [data-ajax]
  $(document).on('click', '[data-ajax]', function(e) {
      e.preventDefault();

      var self = $(this),
          action = self.data('ajax'),
          snippet_params = $.parseJSON( self.data('params').replace(/'/g, '"') ),
          // params = {},
          reload = self.data('ajax-reload'),
          form  = ( $('#' + self.attr('form')).length > 0 ) ? $('#' + self.attr('form').get(0)) : // если у элемента с атрибутом data-ajax есть атрибут form получаем связанную с ним форму
                  ( self.parent('form').length > 0 ? self.parent('form').get(0) : // если элемент лежит непосредственно в форме
                  ( self.parentsUntil('form').length > 0 ) ? self.parentsUntil('form').parent().get(0) : null ); // если элемент лежит на любом уровне вложенности в форме

      // собираем данные с формы
      params = new FormData( form );
      params.append( 'action', action );
      for (key in snippet_params)
          params.append( key, decodeURIComponent(snippet_params[key]) );

      // отправляем запрос
      $.ajax({
          contentType: false, // важно - убираем форматирование данных по умолчанию
          processData: false, // важно - убираем преобразование строк по умолчанию
          dataType: 'json', // тип ожидаемых данных в ответе
          type: 'POST',
          data: params,
          success: function(res) {
              res = $.parseJSON(res);

              // очищаем статусы полей
              form.find('.input.success, .input.error').removeClass('success error');

              var message_timeout = parseInt(res.output.replace(/<[^>]+>/g,'').length) * 75, // задержка в мс зависящая от длины отображаемого сообщения
                  popup = self.parentsUntil('.popup').parent('.popup');

              if ( res.success ) {
                  // выводим сообщение об успешном выполнении запроса
                  form.find('.form__message').removeClass('success error').addClass('success').html(res.output);


                  /* // действия при успешном запросе при определённых action
                  switch ( action ) {
                      case 'auth':
                      case 'logout':
                          // выводим минипрофиль/блок авторизации в шапке сайта
                          $('#header_profile_wrap').html(res.output);

                          // перенаправляем на указанную в res.redirectTo страницу
                          if ( res.redirectTo )
                              window.location = res.redirectTo;
                      break;

                      case 'saveProfile':
                          // обновляем минипрофиль в шапке сайта
                          $('#header_profile_wrap').html(res.header_profile);

                          // выводим сообщение об успешном выполнении
                          form.find('.form__message').removeClass('success error').addClass('success').html(res.output);

                          // через некоторое время (зависящее от длины сообщения) скрываем сообщение
                          setTimeout(function() {
                              form.find('.form__message').text('').removeClass('success error');
                          }, message_timeout);
                      break;
                  }
                  */

                  // чистим список ошибок
                  $('.popup__error-msg').text('');

                  if ( ['reg', 'resetPass'].indexOf(action) == -1 ) { // закрываем попап если это не регистрация и не сброс пароля
                      $.magnificPopup.close();
                  } else {
                      $('.popup__success-msg').text(res.output);
                  }

                  // очищаем поля с паролями
                  form.find('[type="password"]').each(function() {
                      $(this).val('');
                  });
              } else if ( popup && self.parentsUntil('.popup').parent('.popup').length > 0 ) { // если действие вызывалось из попапа и есть ошибки - выводим ошибки в элемент с классом popup__error-msg
                  popup.find('.popup__error-msg').html(res.output);
              } else if ( res.wrong_element_selector ) {
                  // подсвечиваем неправильно заполненные поля или выделяем неправильные элементы
                  if ( !$(res.wrong_element_selector).hasClass(res.wrong_element_css_class ? res.wrong_element_css_class : 'error') )
                      $(res.wrong_element_selector).addClass(res.wrong_element_css_class ? res.wrong_element_css_class : 'error');

                  // выводим сообщения об ошибках
                  form.find('.form__message').removeClass('success error').addClass('error').text(res.output);

                  setTimeout(function() {
                      if ( action == 'saveProfile' && !res.success && res.redirectTo ) // при ошибке сохранения профиля пользователя и указании страницы для переадресации - перенаправляем пользователя
                          window.location = res.redirectTo;
                      else
                          form.find('.form__message').text('').removeClass('success error'); // очищаем сообщения формы
                  }, message_timeout);
              }

              // перезагружаем страницу
              if ( res.reload || (res.success && reload) )
                  window.location.reload();
          }
      });
  });
  // \\ ajax запросы при нажатии на кнопки с [data-ajax]
});

/* действия по успешной отправке формы через AjaxForm */
$(document).on('af_complete', function(event, response) {
    // автоматическое закрытие magnificpopup при успешной отправке формы
    if (response.success)
        $.magnificPopup.close();
});
/* \\ действия по успешной отправке формы через AjaxForm */

/* YANDEX MAPS. Ищет на странице элемент с атрибутом data-map="*адреса разделённые ||*" и вставляет в него карту с маркерами на указанных адресах */
if ( $('[data-map]').length > 0 ) {
    (function maps() {
        var init_map = function($map) {

            var coords_str = $map.data('map'),
            	address_array = coords_str.split('||');

            // расставляем маркеры и центрируем карту
            $.each(address_array, function(idx, addr) {
                ymaps.geocode(addr, {
                    results: 1
                }).then(function(res) {
                	var firstGeoObject = res.geoObjects.get(0),
                        coords = firstGeoObject.geometry.getCoordinates();
                    var placemark = new ymaps.Placemark(coords, {
                    	balloonContent: firstGeoObject.properties._data.balloonContent
                    }, {});

                    if ( typeof myMap !== 'object' ) {
                        myMap = new ymaps.Map($map[0], {
                            center: coords,
                            zoom: 16,
                            controls: ['zoomControl']
                        });

                        myMap.behaviors.disable("scrollZoom");
                    }

                    myMap.geoObjects.add(placemark);

                    if (address_array.length > 1) {
                        myMap.setBounds(myMap.geoObjects.getBounds(), {
                            checkZoomRange: true,
                            preciseZoom: true,
                            zoomMargin: 10
                        });
                    } else {
                        if (address_array.length == 1) {
                            myMap.setZoom(16);
                            myMap.setCenter(coords);
                        }
                    }
                });
            });
        }

        // инициализация яндекс карты
        $.getScript('//api-maps.yandex.ru/2.1?lang=ru_RU', function() {
            ymaps.ready(function() {
                $('[data-map]').each(function() {
                    init_map($(this));
                });
            });
        });
    })();
}
/* \\ YANDEX MAPS */
