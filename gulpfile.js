'use strict'
const syntax        = 'sass'; // Syntax: sass or scss;
const gulp          = require('gulp');
const gutil         = require('gulp-util' );
const sass          = require('gulp-sass');
const concat        = require('gulp-concat');
const uglify        = require('gulp-uglify');
const cleancss      = require('gulp-clean-css');
const rename        = require('gulp-rename');
const autoprefixer  = require('gulp-autoprefixer');
const notify        = require("gulp-notify");
const sourcemaps    = require("gulp-sourcemaps");
const ftp           = require('vinyl-ftp');

var host            = '';
var user            = '';
var pass            = '';
var distCss         = '';
var distJs          = '';

gulp.task('styles', function() {
	return gulp.src('assets/src/styles/unit.sass')
  .pipe(sourcemaps.init())
	.pipe(sass({ outputStyle: 'expand' }).on("error", notify.onError()))
	.pipe(rename({ suffix: '.min', prefix : '' }))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
	.pipe(concat('styles.min.css'))
	.pipe(sourcemaps.write('assets/src/styles'))
  .pipe(gulp.dest('assets/css'));
});

gulp.task('scripts', function() {
	return gulp.src([
		'assets/src/scripts/jquery.min.js',
		'assets/src/scripts/swiper.min.js',
		'assets/src/scripts/magnific_popup.min.js',
		'assets/src/scripts/jquery.maskedinput.min.js',
		'assets/src/scripts/common.js', // Always at the end
		])
	.pipe(concat('scripts.min.js'))
	.pipe(uglify()) // Mifify js (opt.)
	.pipe(gulp.dest('assets/js'));
});

gulp.task('deployCss', function() {
	var conn = ftp.create({
		host: host,
		user: user,
		password: pass,
		parallel: 10,
		log: gutil.log,
	});

	return gulp.src(['assets/css/*'], {buffer: false})
			.pipe(conn.dest(distCss));
});

gulp.task('deployJs', function() {
	var conn = ftp.create({
		host: host,
		user: user,
		password: pass,
		parallel: 10,
		log: gutil.log,
	});

	return gulp.src(['assets/js/*'], {buffer: false})
			.pipe(conn.dest(distJs));
});

gulp.task('watch', function() {
    gulp.watch(['assets/src/styles/*.'+syntax], gulp.parallel('styles'));
    gulp.watch(['assets/src/scripts/*.js'], gulp.parallel('scripts'));

		if(host != '') {
			gulp.watch(['assets/css/*'], gulp.parallel('deployCss'));
			gulp.watch(['assets/js/*'], gulp.parallel('deployJs'));
		}
});

gulp.task('default', gulp.parallel('watch'));
